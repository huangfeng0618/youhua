/* 

    1, 什么是移动web
        跑在手机端的web页面
        跨平台
        基于webview
        告别IE 拥抱 webkit
        更高的适配和性能要求
    2, web前端适配方法
        移动web
            定高，宽度百分比
            flex
            media query
    3, 移动web开发
        1, viewport 和 流式布局
        2, Css flex和media query媒体查询适配
        3, rem原理和适配方法 
        4, 移动端其他适配方法 
    Viewport 
        1，物理像素，css像素， dpr
            dpr=1  表示1px CSS像素代表1px 物理像素
            dpr=2  表示1px CSS像素代表2px 物理像素
        放大或缩小页面，会改变像素比
            页面缩小，1px物理像素上显示更多CSS像素
            页面放大，1px物理像素上显示更更少CSS像素

    layout viewport 布局视窗
        浏览器的初始视窗大小，和浏览器厂商有关
    Visual viewport 物理视窗
        可视区域大小，手机屏幕就是这么大
    Ideal viewport 理想视窗


    4，流式布局
    
    移动端web适配
        Media Query(媒体查询)
        @media 媒体类型(screen, print) and 媒体特性(max-width){

        }
    适配方案
        rem:
            通过js动态设置html的font-size, 将px替换成rem
        vw:
            1vw 等于视口宽度的1% 兼容ios8+ Android4.4+

    移动端的touch事件
        touchstart
        touchmove
        touchend
        touchcancel
    changedTouches , touches , targetTouches的区别？
        touches: 屏幕上所有触摸手指的对象
        targetTouches: 所绑定事件的元素上的手指对象
        changedTouches: 改变touch对象的手指对象，之后触摸的手指

    避免移动端300ms延迟
        1,   <meta name="viewport" content="user-scalable=no"/>
            在safari和移动端webview中不支持
        2,  Tap组件(FastClick, react-tap)
    点击穿透

*/



