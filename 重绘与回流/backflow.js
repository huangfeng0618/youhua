/*
    回流：
        1，当render tree中的一部分（或全部）因为元素的规模尺寸，布局，
        隐藏等改变而需要重新构建。这就称为回流
        2，当页面布局和几何属性改变时就需要回流
            尽量不使用那些CSS属性，通过减少使用触发回流的CSS属性
    
    重绘：
        当render tree中的一些元素需要更新属性，而这些属性只是
        影响元素的外观，风格，而不会影响布局，比如background-color
        就叫称为重绘

    回流必定触发重绘  反之则不是
        避免使用触发重绘、回流的CSS属性
        将重绘、回流的影响范围限制在单独的图层之内
            不能一味地添加图层，图层多了会增加图层的合并时间，反而会影响性能
    触发页面重布局的属性
        1，盒子模型相关属性
        2，定位和浮动
        3，改变节点内部文字结构
    解决回流：
        1，将频繁重绘回流的DOM元素单独作为一个独立的图层，那么这个DOM
        元素的重绘和回流影响只会在这个图层中
            Chrome创建图层的条件
                1, 3D或变换 perspective transform translate3d


    只会触发重绘    
        color，border-style, border-radius, visibility, text-decoration
        background, background-image/position/repeate/size box-shadow
    

    实战优化
        1，translate替换top---top会触发layout过程
            
        2，opacity替换visibility----visibility会触发重绘
        3，使用class一次性修改DOM，
            先把DOM的display:none;然后再
            修改100次，再把它显示出来
        4，不要使用table
        5，合适的动画速度
        6，对动画新建图层，手动将动画新建图层
        7，使用GPU硬件加速

*/