const a1 = {
    r: 123,
    b: {
        c: {
            d: [1,2,3]
        }
    }
}

function clone(source){
    let target = {};
    for( let i in source){
        if (source.hasOwnProperty(i)){
            if (isObject(source[i])){
                target[i] = arguments.callee(source[i])
            } else if(isArray(source[i])) {
                let targetArr = []
                source[i].forEach(function(item, index){
                    targetArr[index] = item
                })
                target[i] = targetArr
            } else {
                target[i] = source[i]
            }
        }
    }
    return target
}

// 判断是否是对象的方法
function isObject(x){
    return Object.prototype.toString.call(x) === '[object Object]'
}
// 判断是否是数组的方法
function isArray(x){
    return Object.prototype.toString.call(x) === '[object Array]'
}

const a2 = clone(a1)

console.log(a1.b.c === a2.b.c)
console.log(a2.b.c.d)

