var express = require('express');
var proxy = require('http-proxy-middleware');
var app = express();

app.use('/',proxy({
    target: 'http://lczm.huayinyiliao.cn',
    changeOrigin: true,

    //修改响应头信息，实现跨域并允许带cookie
    onProxyRes: function(proxyRes, req, res){
        res.header("Access-Control-Allow-Origin", "http://localhost:8080");
        res.header("Access-Control-Allow-Credentials", 'true');
    },
    // 修改响应信息中的cookie域名
    cookieDomainRewrite: 'www.domain1.com' //可以为false，表示不修改
    
}));

app.listen(3000);
console.log('Proxy server is listen at port 3000...');