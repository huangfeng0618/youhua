var $ = {
  ajax: function(options){
    var xhr = null,
        url = options.url,
        method = options.method || 'GET',
        async = typeof options.async === 'undefined' ? true : options.async,
        data = options.data || null,
        params = '',
        callback = options.success,
        error = options.error
    
    if(data){
      for(var i in data){
        params += i + '=' + data[i] + '&'
      }
      params = params.replace(/&$/, "")

    }
    if(options.method === 'get'){
      url += '?' + params
    }
    console.log(async)
    //封装通用xhr对象
    function createXHR(){
      if(typeof XMLHttpRequest != 'undefined'){
        return new XMLHttpRequest()
      } else if(typeof ActiveXObject != 'undefined'){
        var xhrArr = ['Microsoft.XMLHTTP', 'MSXML2.XMLHTTP.6.0','MSXML2.XMLHTTP.5.0']
        var xhr
        for(var i=0, len=xhrArr.length; i<len; i++){
          try{
            xhr = new ActiveXObject(xhrArr[i])
            break;
          } catch(e){

          }
        }
        return xhr
      } else {
        throw new Error('not import xhr object')
      }
    }

    xhr = createXHR()

    xhr.onreadystatechange = function(){
      if(xhr.readyState === 4){
        if(xhr.status >= 200 && xhr.status < 300 || xhr.status === 304){
          var data = JSON.parse(xhr.responseText)
          callback && callback(JSON.parse(xhr.responseText))

        } else {
          error && error()
        }
      }
    }

    xhr.onreadystatechange = function(){
      if(xhr.readyState === 4){
        if(xhr.status >= 200 && xhr.status < 300 || xhr.status === 304){
          
        }
      }
    }

    xhr.open(options.method, options.url, true)
    // post请求头 如果需要在send中传值，则需要设置请求头Content-type发送请求之前对表单数据进行编码
    xhr.setRequestHeader("Content-type", "application/x-www.formurl")
    xhr.send(  )


  },
  jsonp: function(){

  }
}


// $.ajax({
//   url: '',
//   method: 'get',
//   async: true,
//   data: {},
//   success: function(){

//   }
// })


