import React, {Component} from 'react'

export default class Clock extends Component {

  // 固定的名字
  state = {
    date: new Date()
  }

  componentDidMount(){
    setInterval(() => {
      this.setState({
        data: new Date()
      })
    }, 1000)
  }

  componentWillUnmount(){
    
  }

  render () {
    return (
      <div>
        {this.state.date.toLocaleTimeString()}
      </div>
    )
  }
}