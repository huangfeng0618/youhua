import React, {Component} from 'react'

export default class CartSample extends Component {
  constructor(prop){
    super(prop)
    this.state = {
      goods: [
        {name: '001'},
        {name: '002'},
      ]
    }
  }
  addGood(){
    this.setState(prevState =>{
      return {goods: [
        ...prevState.goods,
        {
          id: ''
        }
      ]}
      // prevState.goods.push({
      //   id: ''
      // })
    })
  }
  render(){
    const title = this.props.title ? this.props.title : null
    return (
      <div>{title}</div>
    )
  }
}