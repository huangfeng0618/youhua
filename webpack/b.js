function b(){
  console.log('b')
}
export default b

// import a from './a'
// import b from './b'
// import axios from 'axios'

// import pic from './img/timg.jpg'
// import './index.css'
// import './index.scss'
// const img = new Image()
// img.src = pic
// img.classList.add('pic')
// document.querySelector("#root").append(img)
// // 既然webpack只认识js模块，遇到非js模块该怎么办
// a()
// b()
// if(module.hot){
//   module.hot.accept('./a', ()=>{
//     console.log('有更新')
//     a()
//   })
// }
// axios.get('/api/info').then(res => {
//   console.log(res)
// })

// function k(){
//   console.log('hello k')
// }

// k()


/**
 * webpack
 *  模块打包器
 *  默认只能打包js文件
 * webpack配置文件 webpack.config.js
 * 
 */
