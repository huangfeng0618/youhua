const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
// const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const webpack = require('webpack')
module.exports = {
  mode: 'development',
  entry: "./index.js",
  output:{
    path: path.resolve(__dirname, './build'),
    filename:'index.js'
  },
  devtool: 'cheap-module-eval-source-map', // 开发环境配置
  // devtool: 'cheap-module-source-map', // 线上环境配置
  module: {
    // 遇到不认识的模块就在这里写，使用loader解决
    rules: [
      {
        test: /\.(jpe?g|png|gif)$/,
        use: {
          loader: 'url-loader',
          options: {
            // name是打包前的文件名称 ext是打包前的模块格式
            name: "[name]_[hash].[ext]",
            outputPath: "images/",
            limit: 2048
          }
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader", "postcss-loader"] // MiniCssExtractPlugin.loader
      },
      {
        test: /\.scss$/, // loader是有执行顺序的，从后往前执行
        use: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          // options: {
          //   presets: [['@babel/preset-env', {
          //     useBuiltIns: "usage", // 按需加载，试验功能
          //     corejs:2
          //   }]],
          //   // plugins: ["@babel/plugin-transform-runtime"]
          // }
        }
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html',
      title: '自定义标题'
      // filename: 'app.html'
    }),
    // 打包之前将生成目录删除
    new CleanWebpackPlugin(),
    // new MiniCssExtractPlugin({
    //   filename: '[name].css'
    // }),
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    contentBase: './build',
    open: true,
    port: '8081',
    proxy: {
      '/api': {
        target: 'http://localhost:9092'
      }
    },
    hot: true,
    hotOnly: true
  }
}