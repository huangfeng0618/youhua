/**
    Defferred 触发resolve或reject
    Promise 中的resolve或reject后应该做什么
        var deferred = $.Deferred();
        var promise = new Promise()
    $.ajax返回的是一个promise对象，实际上返回的是jqXHR对象。但jqXHR实现了jQuery的Promise接口，
    所以也是一个Promise对象

    
 */

//  一般情况下执行ajax

$("#theButton").prop({
    disabled: true
});

var jqxhr = $.ajax('do/example', {
    type: "post",
    dataType: "json",
    data: getFormData()
})
