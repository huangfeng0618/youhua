/*

    url-->浏览器-->dns服务器-->局域网，交换机，路由器，主干网
    -->服务端-->Controller-->Model-->db-->View-->浏览器渲染，dom树，css树，整合
        1，dns服务器缓存
        2，CDN缓存，CDN是请求静态资源用的，如果CDN的域名和网络域名相同，请求
            静态资源的时候也会携带cookie，造成无谓的损耗，CDN的域名不要和主站
            域名一样，解决静态资源，网络选择，缓存的问题
        3，减少HTTP请求大小
        4，减少HTTP请求数量
        5，服务端渲染
    深入理解http请求的过程是前端优化的核心
        1，资源的合并与压缩：
            html压缩
                nodejs提供的html-minifier工具
                后端模板引擎渲染压缩
            css压缩
                无效代码删除
                css语义合并
                使用clean-css
            js的压缩和混乱
                无效字符删除
                剔除注释
                代码语义的缩减和优化
                代码保护
                使用uglifyjs2对js进行压缩
            文件合并
                首屏渲染
                缓存失效
                    
            开启gzip

    CS架构
        开发 打包发布 运行(用户需要从应用商城下载安装包)
            在用户端解压应用，最终部署在本地的操作系统上
            静态资源已经存在于本地上，用户访问的是本地的资源
    BS架构
        开发    发布(到服务器端)    运行(用户打开浏览器访问服务器的网站) 
            这时浏览器才开始向服务器请求资源，动态的加载服务器资源
    

    图片压缩
        jpg有损压缩，压缩率高，不支持透明
        png支持透明，浏览器兼容好
        webp压缩程度更好，在iOS webview有兼容性问题
        svg矢量图，代码内嵌，相对较小，图片样式相对简单
    进行图片压缩
        舍弃一些相对无关紧要的色彩信息
            png8 256种颜色表达图片 png24 2^24颜色图片 png32
        css雪碧图 在线 www.spritecow.com
            减少HTTP请求数量
            图片较大时，一次加载比较慢
        Image inline
            base64格式
            小图片实际请求消耗是在网络请求过程中
        使用矢量图
            使用svg矢量图
            使用iconfont解决icon问题
        在线图片压缩，智图（图片转换），tinypng
    

    CSS和JS的装载与执行
        HTML渲染过程
            顺序执行、并发加载：单个域名并发请求有限
                HTTP引入外部资源是并发请求的
            是否阻塞
                CSS阻塞
                    css在head中link引入会阻塞页面的渲染
                    css会阻塞js的执行
                    css不会阻塞js的加载
                JS阻塞
                    直接引入的js阻塞页面的渲染 通过的deff和async
                    js不阻塞资源的加载
                    js顺序执行，阻塞后续js逻辑的执行   
            依赖关系

            引入关系
    
    懒加载和预加载
        懒加载
            图片进入可视区域之后请求图片资源
            对于电商等图片很多，页面很长的业务场景试用
            减少无效的资源加载
            并发加载的资源过多会阻塞js的加载
        实现：
            监听scroll事件
        预加载
            图片等静态资源在使用之前提前请求
            资源使用时能从缓存中加载，提升用户体验
            页面展示的依赖关系维护
        
*/ 


console.log("test is run")