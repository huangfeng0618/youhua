self.addEventListener("install", function(event){
    event.waitUntil(
        caches.open("app-v1")
            .then(function(cache){
                console.log("open cache")
                return cache.addAll([
                    './app.js',
                    './main.css',
                    './serviceWorkers.html'
                ])
            })
    )
})

self.addEventListener("fetch", function(event){
    event.respondWith(
        caches.match(event.request)
            .then(function(res){
                if (res){
                    return res
                } else {
                    console.log(event)
                    // 通过fetch方法向网络发起请求
                    // fetch(event.request.url).then(function(){
                    //     if (res){
                    //         // 对于新请求的资源存储道cachestorage中
                    //     }
                    // })
                }
            })
    )
})

