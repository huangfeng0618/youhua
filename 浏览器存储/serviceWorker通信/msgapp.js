if (navigator.serviceWorker){
    var sendBtn = document.getElementById("send-msg-button")
    var value = document.getElementById("msg-input").value
    sendBtn.addEventListener("click", function(){
        // 主页面发送信息到serviceworker
    })
    navigator.serviceWorker.register("./msgsw.js", {scope: './'})
        .then(function(reg){
            console.log(reg)
        })
        .catch(function(e){
            console.log(e)
        })
}else{
    console.log("Server worker is not supported")
}

/*
    主页面向serviceworker发消息
    serviceworker向主页面发消息
*/ 