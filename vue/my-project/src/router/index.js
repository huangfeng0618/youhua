import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Demo1 from '@/pages/demo1'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/demo1',
      name: 'demo1',
      component: Demo1
    },
    {
      path: '/demo2/:id',
      name: 'demo2',
      component: resolve => require(["@/pages/demo2"], resolve)
    },
    {
      path: '/demo10',
      name: 'demo10',
      component: resolve => require(['@/pages/demo10'], resolve)
    },
    {
      path: '/demo14',
      name: 'demo14',
      component: resolve => require(['@/pages/demo14'], resolve)
    },
    {
      path: '/home',
      name: 'home',
      component: resolve => require(['@/pages/home'], resolve)
    }
  ]
})
