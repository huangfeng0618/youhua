import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/page/Home.vue'
import List from '@/components/page/List.vue'
import Cart from '@/components/page/Cart.vue'
import Personal from '@/components/page/Personal.vue'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',

      component: Home
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/list',
      name: 'list',
      component: List
    },
    {
      path: '/cart',
      name: 'cart',
      component: Cart
    },
    {
      path: '/personal',
      name: 'personal',
      component: Personal
    }
  ]
})
