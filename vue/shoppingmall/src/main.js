// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// 导入全部组件
// import Vant from 'vant'
// import 'vant/lib/index.css
// 使用babel-plugin-import插件实现按需导入 
import {Icon, Tabbar, TabbarItem} from 'vant'

Vue.use(Icon).use(Tabbar).use(TabbarItem)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

let w = document.documentElement.clientWidth || document.body.clientWidth;
document.documentElement.style.fontSize = w/640 * 100 + 'px'

