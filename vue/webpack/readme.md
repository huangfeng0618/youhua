1.初始化项目
npm init -y

2.安装webpack 开发依赖 一般使用3.6.0
npm i webpack@3.6.0 --save-dev   开发依赖
npm i webpack@3.6.0 -D

3.安装babel 以及认识es2015 以及 stage-0 预设  识别es6-es7
npm i babel babel-core babel-loader --save-dev   开发依赖
npm i babel-preset-es2015 babel-preset-stage-0 --save-dev    开发依赖

4.设置 .babelrc 文件，设置预设
{
  "preset": ["es2015", "stage-0"]
}

5.安装识别和转义css less
npm i css-loader style-loader --save-dev
npm i less-loader style-loader --save-dev

6.识别文件路径的  例如  导入图片
npm i file-loader url-loader --save-dev

7.安装识别html插件
npm i html-webpack-plugin --save-dev

8.安装webpack服务
npm i webpack-dev-server --save-dev

9.配置脚本 webpack-config.js  package.json中的脚本
"scripts":{
  "build": "webpack",
  "dev": "webpack-dev-server --open"
}

10.配置webpack.config.js文件
let HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  entry: "./src/main.js",
  output: {
    filename: "build.js",
    path: __dirname + "/dist"
  },
  module: {
    rules:[
      {
        test: /\.js$/,
        use: ["babel-loader"],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ["style-loader","css-loader"]
      },
      {
        test: /\.less$/,
        use: ["style-loader","css-loader","less-loader"]
      },
      {
        test: /\.(jpg|gif|png|jpeg)$/i,
        use: ["url-loader?limit=5120"]
      }
    ]
  },
  plugin: [
    new HtmlWebpackPlugin({
      filename: 'zf.html',
      template: './src/index.html'
    })
  ]
}

