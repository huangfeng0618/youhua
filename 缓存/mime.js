
exports.types = {
    'jpg': 'image/jpeg',
    'json': 'application/json',
    'png': 'image/png',
    'text': 'text/plain'
}