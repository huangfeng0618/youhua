const puppeteer = require("puppeteer")
const sleep = time => new Promise(resolve => setTimeout(resolve, time))
puppeteer.launch({
    // headless: false
}).then(async browser => {
    const page = await browser.newPage()
    await page.setRequestInterception(true)
    page.on("request", async interceptedRequest => {
        // interceptedRequest.url().endsWith('.jpg')
        if (interceptedRequest.url().indexOf('.jpg') != -1 || interceptedRequest.url().indexOf('.png') != -1){
            interceptedRequest.abort();
            // console.log(interceptedRequest.url())
        } else {
            await interceptedRequest.continue()
            // console.log(interceptedRequest.response().url())
            // console.log(interceptedRequest.url())
        }
    })
    // 监听响应
    page.on('response', async response => {
        console.log(response.url())
    })
    const response = await page.goto('https://music.163.com')
    
    // await sleep(3000)
    // console.log(response.status())
    await browser.close()
})