const puppeteer = require('puppeteer');

(async() => {

    const browser = await puppeteer.launch({
        headless: false
    })
    const page = await browser.newPage();
    await page.setViewport({width: 1200, height:1000});
    await page.goto('https://weibo.com/rmrb');
    await page.waitForNavigation();
    await page.screenshot({path: 'rmrb.png', type: 'png'})
    browser.close();
})();