const puppeteer = require('puppeteer')
const fs = require('fs')

puppeteer.launch().then(async browser => {
    const page = await browser.newPage()
    page.on("console", msg => console.log(msg.text()))
    await page.exposeFunction('readfile', async filePath => {
        return new Promise((resolve, reject) => {
            fs.readFile(filePath, 'utf-8', (err, text) => {
                if(err) {
                    reject(err)
                } else {
                    resolve(text)
                }
            })
        })
    })

    await page.evaluate(async () => {
        // 使用window.readfile 读取文件内容
        const content = await window.readfile('./md5.js')
        console.log(content)
    })
    await browser.close()
})